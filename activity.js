// 1. What directive is used by Node.js in loading the modules it needs?

require

// 2. What Node.js module contains a method for server creation?

createServer

// 3. What is the method of the http object responsible for creating a server using Node.js?

writeHead

// 4. What method of the response object allows us to set status codes and content types?
	
response status codes 

// 5. Where will console.log() output its contents when run in Node.js?

localhost

// 6. What property of the request object contains the address's endpoint?
request.url